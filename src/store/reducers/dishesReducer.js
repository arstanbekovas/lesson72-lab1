import * as actionTypes from "../actions/actionTypes";


const initialState = {
    dishes: {}
};

const reducer = (state=initialState, action) => {
    switch (action.type) {
        case actionTypes.DISH_SUCCESS:
            console.log(action.info );
            return {dishes: action.info};
        case actionTypes.DELETE_DISH:

            return {};
        case actionTypes.ADD_NEW_DISH:

            return {};
        case actionTypes.EDIT_DISH:

            return {};
        default:
            return state;
    }

};

export default reducer;