import * as actionTypes from '../actions/actionTypes';
import {EDIT_DISH} from "../actions/actionTypes";


const INITIAL_DISH = {
    americana: 0,
    chili: 0,
    pepperoni: 0
};
const INITIAL_PRICE = 20;

const initialState = {
    ingredients: INITIAL_DISH,
    totalPrice: INITIAL_PRICE,
};
const DISH_PRICES = {
    americana: 0,
    chili: 0,
    pepperoni: 0
};
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.ADD_NEW_DISH:
            return{
                ...state,
                dish:{
                    ...state.dish,
                    [action.dishName]: state.dish[action.dishName] + 1
                },
                totalPrice: state.totalPrice + DISH_PRICES[action.dishName]
            };
        case actionTypes.DELETE_DISH:
            return{
                ...state,
                dish: {
                    ...state.dish,
                    [action.dishName]: state.dish[action.dishName] -1
                },
                totalPrice: state.totalPrice - DISH_PRICES[action.DISHName]
            };
        case actionTypes.EDIT_DISH:
            return{...state, dish: EDIT_DISH, totalPrice: INITIAL_PRICE};
        default:
            return state;
    }
};
export default reducer;