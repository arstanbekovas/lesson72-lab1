import * as actionTypes from './actionTypes';
import axios from "../../axios-pizza";


export const dishRequest = () => {
    return {type: actionTypes.DISH_REQUEST};
};
export const dishSuccess = (info) => {
    return {type: actionTypes.DISH_SUCCESS, info};
};

export const dishError = (error) => {
    return {type: actionTypes.DISH_ERROR, error};
};


export const showDishes = () => {
    return dispatch => {
        dispatch(dishRequest());
        axios.get('/dishes.json').then((response) => {
            console.log(response.data);
            dispatch(dishSuccess(response.data));
        }, error => {
            dispatch(dishError(error));
        });
    }
};
export const deleteDish = (id) => {
    return dispatch => {
        dispatch(dishRequest());
        axios.delete(`/dishes/${id}.json`).then((response) => {
            console.log(response.data);
            dispatch(dishSuccess(response.data));
        }, error => {
            dispatch(dishError(error));
        });
    }
};


export const addNewDish = (info) => {
    return dispatch => {
        dispatch(dishRequest());
        axios.post('/dishes.json', info).then((response) => {
            console.log(response.data);
            dispatch(dishSuccess(response.data));
        }, error => {
            dispatch(dishError(error));
    });
}};

export const editDish = (id) => {
    return dispatch => {
        dispatch(dishRequest());
        axios.patch(`/dishes/${id}.json`).then((response) => {
            console.log(response.data);
            dispatch(dishSuccess(response.data));
        }, error => {
            dispatch(dishError(error));
        });
    }
}

