export const ADD_NEW_DISH = 'ADD_NEW_DISH';
export const DELETE_DISH = 'DELETE_DISH';
export const EDIT_DISH = 'EDIT_DISH';


export const DISH_REQUEST = 'DISH_REQUEST';
export const DISH_SUCCESS = 'DISH_SUCCESS';
export const DISH_ERROR = 'DISH_ERROR';
