import * as actionTypes from './actionTypes';
import axios from "../../axios-pizza";

export const addToCart = (menuName, price) => {
    return {type: actionTypes.ADD_NEW_DISH, menuName, price};
};

export const removeFromCart = menuName => {
    return {type: actionTypes.DELETE_DISH, menuName};
};

export const placeOrder = menuName => {
    return {type: actionTypes.EDIT_DISH, menuName};
};


export const orderRequest = () => {
    return {type: actionTypes.DISH_REQUEST};
};

export const orderSuccess = (info) => {
    return {type: actionTypes.DISH_SUCCESS, info};
};

export const orderError = (error) => {
    return {type: actionTypes.DISH_ERROR};
};


export const fetchDish = () => {
    return dispatch => {
        dispatch(orderRequest());
        axios.get('/dishes.json').then((response) => {
            console.log(response.data, 'RESPONSE DATA');
            dispatch(orderSuccess(response.data));
        }, error => {
            dispatch(orderError());
        });
    }
};


