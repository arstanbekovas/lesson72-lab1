import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://lesson-72.firebaseio.com/'
});

export default instance;