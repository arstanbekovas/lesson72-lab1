import React from 'react';
import './DishControl.css';

const DishControl = props => {
    return (
        <div className='DishControl'>
            <img className='pic' src={props.URL}/>
            <div className='dish'>{props.dish}</div>
            <div className='price'>{props.price} KGS</div>
            <button onClick={props.delete} className='Delete'>Delete</button>
        </div>
    )
};

export default DishControl;