import React, {Component} from 'react';
import axios from 'axios';
import './EditDish.css'
import {editDish, showDishes} from "../../../store/actions/dishBuilder";
import {connect} from "react-redux";

class EditDish extends Component {
    state = {
        dishes: [],
        loading: false
    };

    componentDidMount() {
        const id = this.props.match.params.id;
        this.props.onShowDishes();
    }

    dishValueChanged = e => {
        e.persist();
        this.setState(prevState => {
            return {
                dish: {...prevState.dish, [e.target.name]: e.target.value}
            }
        });
    };

    render() {
        return (
            <form className="EditQuote">
                <h1>Edit a dish</h1>
                <input type="text" name="name" placeholder="Enter dish"
                       value={this.props.dish} onChange={this.dishValueChanged}/>
                <input name="price" placeholder="Enter price"
                       value={this.props.price} onChange={this.dishValueChanged}
                />
                <input name="ulr" placeholder="Enter image url"
                       value={this.props.url} onChange={this.dishValueChanged}
                />
                <button onClick={this.onDishEdited}>Save</button>
            </form>
        )
    }
}

const mapStateToProps = state => {
    return {
        dish: state.dishes.name,
        price: state.dishes.price,
        url: state.dishes.url
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onDishEdited: (dishName) => dispatch(editDish(dishName)),
        onShowDishes: () => dispatch(showDishes())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(EditDish);