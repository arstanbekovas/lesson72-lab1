import React, {Component} from 'react';
import {connect} from "react-redux";

import './NewDish.css';
import {addNewDish} from "../../../store/actions/dishBuilder";

class NewDish extends  Component {
    state = {
        dish: [],
        loading: false
    };

    dishCreateHandler = e => {
       this.props.onDishAdded();
       this.props.history.replace('/');
    };

    dishValueChanged = e => {
        e.persist();
        this.setState(prevState => {
            return {
                dish: {...prevState.dish, [e.target.name]: e.target.value}
            }
        });
    };

    render(){
        return(
            <form className="DishAdd">
                <input type="text" name="name" placeholder="Enter dish"
                       value={this.state.dish.name} onChange={this.dishValueChanged}/>
                <input name="price" placeholder="Enter price"
                          value={this.state.dish.price} onChange={this.dishValueChanged}
                />
                <input name="ulr" placeholder="Enter image url"
                       value={this.state.dish.url} onChange={this.dishValueChanged}
                />
                <button onClick={this.dishCreateHandler}>Create</button>
            </form>
        )
    }
}


const mapStateToProps = state => {
    return {
        dish: state.dishes.name,
        price: state.dishes.price,
        url: state.dishes.url
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onDishAdded: (dishName) => dispatch(addNewDish(dishName)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(NewDish);