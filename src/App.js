import React, { Component, Fragment } from 'react';
import './App.css';
import {NavLink, Route, Switch} from "react-router-dom";
import NewDish from "./components/Dishes/NewDish/NewDish";
import DishesList from "./containers/DishesBuilder/DishesList";
import EditDish from "./components/Dishes/EditDish/EditDish";
import Dish from "./containers/Dish/Dish";

class App extends Component {
  render() {
    return (
        <Fragment>
            <nav className="App-nav">
                <ul>
                    <li><NavLink to="/" exact>Dishes</NavLink></li>
                    <li><NavLink to="/dishes" exact>Orders</NavLink></li>
                </ul>
            </nav>

            <Switch>
                <Route path="/" exact component={DishesList}/>
                <Route path="/add" component={NewDish}/>
                <Route path="/edit" component={EditDish}/>
                <Route path="/dishes" component={Dish} />
                <Route render={() =>  <h1>Not Found</h1>}/>
            </Switch>
        </Fragment>
    );
  }
}

export default App;
