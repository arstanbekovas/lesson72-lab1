import React, {Component, Fragment} from 'react';
import './DishesList.css';
import DishControl from "../../components/Dishes/DishControl/DishControl";
import {connect} from 'react-redux';
import { deleteDish, editDish, showDishes} from "../../store/actions/dishBuilder";
import {NavLink} from "react-router-dom";



class DishesList extends Component {

    componentDidMount() {
        this.props.onShowDishes();
    }

    dishEditHandler = (id) => {
        this.props.onDishEdited();
        this.props.history.replace('/edit');
    };

    render() {
        return (
            <Fragment>
                <h1>Dishes</h1>

                <NavLink className='button' to={'/add'}>Add new Dish</NavLink>
                <div>
                    {Object.keys(this.props.dishes).map(dishId => (
                        <DishControl
                            key={dishId}
                            name={this.props.dishes[dishId].name}
                            price={this.props.dishes[dishId].price}
                            pic={this.props.dishes[dishId].url}
                            delete={() => this.props.onDishDeleted(dishId)}
                            edit={() => this.dishEditHandler(dishId)}
                        />
                    ))}
                </div>
            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        dishes: state.dishes.dishes
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onDishDeleted: (dishId) => dispatch(deleteDish(dishId)),
        onDishEdited: (dishName) => dispatch(editDish(dishName)),
        onShowDishes: () => dispatch(showDishes())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(DishesList);