import React, {Fragment, Component} from 'react'
import './Dish.css'
import {connect} from "react-redux";
import {fetchDish} from "../../store/actions/dishOrder";
import DishControl from "../../components/Dishes/DishControl/DishControl";

class Dish extends Component {

    componentDidMount() {
        this.props.fetchDish()
    }

    deleteDish = dish => {
        console.log(dish)
    };

    render() {
        return (
            <Fragment>

                {Object.keys(this.props.dishes).map((dish, index) =>
                   <DishControl key={index}
                                dish={dish}
                                price={this.props.dishes[dish].Price}
                                URL={this.props.dishes[dish].URL}
                                delete={this.deleteDish} />
                )}
            </Fragment>
        )
    }

}

const mapStateToProps = state => state.dishes;


const mapDispatchToProps = dispatch => ({
    fetchDish: () => dispatch(fetchDish())
});


export default connect(mapStateToProps, mapDispatchToProps)(Dish);
